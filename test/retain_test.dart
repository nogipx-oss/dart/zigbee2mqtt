import 'package:mqtt_client/mqtt_client.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:zigbee2mqtt/zigbee2mqtt.dart';


class MockZigbeeClient extends Mock implements Zigbee2Mqtt {} 

void main() async {

  final requestTopic = 'zb/topic/get';
  final responseTopic = 'zb/topic';
  final qos = MqttQos.exactlyOnce;
  
  Zigbee2Mqtt client = Zigbee2Mqtt(
    host: '192.168.1.2',
    clientId: 'mac'
  );

  Zigbee2Mqtt server = Zigbee2Mqtt(
    host: '192.168.1.2',
    clientId: 'zigbee'
  );

  test('Test request method', () async {
    // client.logging(on: true);
    server.logging(on: true);
    
    await client.connect();
    await server.connect();
    
    server.subscribe(requestTopic, qos);
    server.updates.listen((data) {
      final messages = data.map((e) => e.publishedMessage).toList();
      // if (messages.any((e) => e.variableHeader.topicName == requestTopic)) {
        
      // }
      client.send(
          message: MqttPublishMessage()
            ..withQos(qos)
            ..toTopic(responseTopic)
            ..setPayload('Hello user bip-bop 129031')
        );

      print('[SERVER] $messages');
    });

    final result = await client.request<String>(
      responseTopic: responseTopic, 
      responseMapper: (v) => v,
      requestMessage: MqttPublishMessage()
        ..withQos(qos)
        ..toTopic(requestTopic)
        ..setPayload('HI BITCH')
    );
    print('Result $result');
    return result != null;
  });
}