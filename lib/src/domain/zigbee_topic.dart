import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'zigbee_topic.g.dart';


class ZigbeeTopic extends EnumClass {
  static const _default = 'zigbee2mqtt';

  const ZigbeeTopic._(String name) : super(name);
  static BuiltSet<ZigbeeTopic> get values => _$values;
  static ZigbeeTopic valueOf(String name) => _$valueOf(name);

  static Serializer<ZigbeeTopic> get serializer => _$zigbeeTopicSerializer;

  @BuiltValueEnumConst(wireName: '$_default/bridge/config/devices')
  static const ZigbeeTopic allDevices = _$wireAllDevices;

}

