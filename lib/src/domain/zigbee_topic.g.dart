// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'zigbee_topic.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ZigbeeTopic _$wireAllDevices = const ZigbeeTopic._('allDevices');

ZigbeeTopic _$valueOf(String name) {
  switch (name) {
    case 'allDevices':
      return _$wireAllDevices;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ZigbeeTopic> _$values =
    new BuiltSet<ZigbeeTopic>(const <ZigbeeTopic>[
  _$wireAllDevices,
]);

Serializer<ZigbeeTopic> _$zigbeeTopicSerializer = new _$ZigbeeTopicSerializer();

class _$ZigbeeTopicSerializer implements PrimitiveSerializer<ZigbeeTopic> {
  static const Map<String, String> _toWire = const <String, String>{
    'allDevices': 'zigbee2mqtt/bridge/config/devices',
  };
  static const Map<String, String> _fromWire = const <String, String>{
    'zigbee2mqtt/bridge/config/devices': 'allDevices',
  };

  @override
  final Iterable<Type> types = const <Type>[ZigbeeTopic];
  @override
  final String wireName = 'ZigbeeTopic';

  @override
  Object serialize(Serializers serializers, ZigbeeTopic object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ZigbeeTopic deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ZigbeeTopic.valueOf(_fromWire[serialized] ?? serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
