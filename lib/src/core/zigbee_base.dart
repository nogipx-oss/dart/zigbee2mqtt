import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:meta/meta.dart';

class Zigbee2Mqtt extends MqttServerClient {
  final String host;
  final String clientId;
  final String user;
  final String password;

  Zigbee2Mqtt({
    @required this.host, 
    this.clientId = '',
    this.user, 
    this.password, 
  }): super(host, clientId);

  

}