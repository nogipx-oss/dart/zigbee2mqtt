
import 'dart:async';
import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:typed_data/typed_buffers.dart' as buffers;


extension ExtMqttClient on MqttClient {
  int send({
    @required MqttPublishMessage message,
    bool retain = false
  }) {
    if (message.variableHeader.topicName == null) {
      throw ArgumentError.notNull('topic');
    }
    if (message.header.qos == null) {
      throw ArgumentError.notNull('qos');
    }
      
    return publishMessage(
      message.variableHeader.topicName, 
      message.header.qos, 
      message.payload.message ?? buffers.Uint8Buffer(), 
      retain: retain
    );
  }

  /// Get retained message from broker
  /// Subscribes to topic and unsubscribes
  /// after getting message
  Future<R> request<R>({
    @required String responseTopic, 
    @required R Function(String payload) responseMapper,
    @required MqttPublishMessage requestMessage,
  }) async {
    final receiveCompleter = Completer<R>();

    subscribe(responseTopic, MqttQos.exactlyOnce);
    final sub = updates.listen((data) async {
      final messages = data.map((e) => e.publishedMessage).toList();
      receiveCompleter.complete(responseMapper(messages[0].payloadString));
    }, cancelOnError: true);

    send(message: requestMessage);
    return await receiveCompleter.future.then((value) async {
      unsubscribe(responseTopic);
      await sub.cancel();
      return value;
    });
  }
}


mixin MqttEntity<T> {
  Map<String, dynamic> toJson() =>
    throw UnimplementedError();

  static T fromJson<T>(String json) => 
    throw UnimplementedError();

  MqttPublishMessage toMqttMessage({
    String topic, bool retain, MqttQos qos
  }) {
    final message = MqttPublishMessage()
      ..publishData(utf8.encode(jsonEncode(toJson())));

    if (topic != null) message.toTopic(topic);
    if (retain != null) message.setRetain(state: retain);
    if (qos != null) message.withQos(qos);

    return message;
  }

  T fromMqttMessage(MqttPublishMessage message) =>
    throw UnimplementedError();
}


extension ExtMqttPublishMessage on MqttPublishMessage {
  MqttPublishMessage setPayload(String data) => publishData(
    buffers.Uint8Buffer()..addAll(utf8.encode(data))
  );

  String get payloadString => 
    MqttPublishPayload.bytesToStringAsString(payload.message);

  Map<String, dynamic> get payloadJsonOrNull {
    try { return jsonDecode(payloadString); } 
    catch (e) { 
      print(e);
      return null; 
    }
  }
}


extension ExtMqttReceivedMessage on MqttReceivedMessage {
  MqttPublishMessage get publishedMessage => 
    payload as MqttPublishMessage;
}

